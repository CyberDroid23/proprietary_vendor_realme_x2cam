# Permissions
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/configs/permissions/com_oppo_camera_unit_sdk.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.oppo.camera.unit.sdk.xml \
    $(LOCAL_PATH)/configs/permissions/privapp-permissions-oppo.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-oppo.xml \
    $(LOCAL_PATH)/configs/permissions/privapp-permissions-oppo.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/privapp-permissions-oppo.xml \
    $(LOCAL_PATH)/configs/permissions/privapp-permissions-oppo-ext.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-oppo-ext.xml

# Oplus framework
# PRODUCT_PACKAGES += \
#    oplus-fwk

PRODUCT_BOOT_JARS += \
#    oplus-fwk \
    coloros-support-wrapper

TARGET_USES_OPLUS_CAMERA := true

# Properties
PRODUCT_PRODUCT_PROPERTIES += \
    ro.oplus.system.camera.name=com.oppo.camera \
    ro.com.google.lens.oem_camera_package=com.oppo.camera \

# Sepolicy
#BOARD_VENDOR_SEPOLICY_DIRS += \
#    vendor/realme/X2camera/sepolicy/vendor

#SYSTEM_EXT_PRIVATE_SEPOLICY_DIRS += \
#    vendor/realme/X2camera/sepolicy/private

$(call inherit-product, vendor/realme/X2camera/camera-vendor.mk)

